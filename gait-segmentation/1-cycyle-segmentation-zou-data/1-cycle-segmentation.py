import csv
import os
import sys
dir = '/Users/hassaanabid/Documents/SCHOOL/EECS4088-project/dataset/zou/Dataset#1'

train_dir = os.path.join(dir, 'train')
train_dir_sen = os.path.join(dir, 'train', 'InertialSignals')
f0 = os.path.join(train_dir, 'y_train.txt')
f1 = os.path.join(train_dir_sen, 'train_acc_x.txt')
f2 = os.path.join(train_dir_sen, 'train_acc_y.txt')
f3 = os.path.join(train_dir_sen, 'train_acc_z.txt')
f4 = os.path.join(train_dir_sen, 'train_gyr_x.txt')
f5 = os.path.join(train_dir_sen, 'train_gyr_y.txt')
f6 = os.path.join(train_dir_sen, 'train_gyr_z.txt')

data = []
with \
    open(f0) as g0, \
    open(f1) as g1, \
    open(f2) as g2, \
    open(f3) as g3, \
    open(f4) as g4, \
    open(f5) as g5, \
    open(f6) as g6 \
    :
    for l0,l1,l2,l3,l4,l5,l6 in zip(g0,g1,g2,g3,g4,g5,g6):
        row = [int(l0),l1,l2,l3,l4,l5,l6]
        data.append(row)

dic = {}
for i in data:
    k = int(i[0])
    val = i[1:]
    if k not in dic:
        dic[k] = {
            'train_acc_x' : [],
            'train_acc_y' : [],
            'train_acc_z' : [],
            'train_gyr_x' : [],
            'train_gyr_y' : [],
            'train_gyr_z' : [],
        }
    item = dic[k]
    item['train_acc_x'].append(val[0])
    item['train_acc_y'].append(val[1])
    item['train_acc_z'].append(val[2])
    item['train_gyr_x'].append(val[3])
    item['train_gyr_y'].append(val[4])
    item['train_gyr_z'].append(val[5])
    dic[k] = item


for k,dic2 in dic.items():
    for k2, v2 in dic2.items():
        # doubel overlap
        # i = 0
        # new_v2 = []
        # while i < len(v2)-1:
        #     new_v2.append(
        #         v2[i].replace("\n"," ") + v2[i+1].replace("\n"," " + '\n')
        #     )
        #     i += 2
        # dic2[k2] = new_v2

        # half overlap
        i = 0
        new_v2 = []
        for j in v2:
            tmp_j = list(j.split(' '))
            tmp_j_1 = tmp_j[:64]
            # tmp_j_2 = tmp_j[64:]
            new_v2.append(' '.join(tmp_j_1) + '\n')
            # new_v2.append(' '.join(tmp_j_2) + '\n')
        dic2[k2] = new_v2
        



for k,v in dic.items():
    for k2, v2 in v.items():
        with open('./' + k2 + '.txt', 'a+') as wx:
            for l in v2:
                wx.write(l)
                if k2 == 'train_acc_x':
                    with open('./' + 'y_train' + '.txt', 'a+') as ax:
                        ax.write(str(k)+'\n')

       