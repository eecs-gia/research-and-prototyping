import pandas as pd


def sampling_and_interpolation(filename, ms):
    """
        for each item in samples_list call this method
    """
    DECIMAL_PLACES = 8  # per row for dataset#1
    res = []

    df = pd.read_csv(filename, sep=",", squeeze=True)

    userID = df['userID'].iloc[0]

    df['timeMs'] = pd.to_datetime(df['timeMs'], unit='ms')
    df.timeMs = df.timeMs - pd.Timedelta('05:00:00')

    df_accX = df
    resampled_df_accX = df_accX.set_index('timeMs').resample(ms).mean().interpolate()

    dfList_accX = list(resampled_df_accX['vSum'])
    dfList_time = resampled_df_accX.index.tolist()

    resampled_df_accX = resampled_df_accX.reset_index()

    resampled_df_accX.index += 1  # start from 1
    resampled_df_accX.index.name = 'index'

    df_vsum_only = resampled_df_accX[['vSum']].copy()

    del resampled_df_accX['index']

    if 'userID' not in resampled_df_accX.columns:
        resampled_df_accX.insert(0, 'userID', ' ')

    # print(resampled_df_accX)
    filename1 = 'si_vsum_' + filename;
    df_vsum_only.to_csv(filename1, sep=',', float_format='%.6f')
    print(f"si format=[index, vSum] \t {filename1}")

    filename2 = 'si_' + filename
    resampled_df_accX.to_csv(filename2, sep=',', float_format='%.6f')
    print(f"si peaks full row \t {filename2}")

    return (dfList_time, dfList_accX)
