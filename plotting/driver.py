# usage : python3 driver.py filename.csv

from argparse import ArgumentParser

import findPeaks as fp
import sampling_interpolation as si
from plot import Plot


# argument parser
def parse_args():
    parser = ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    return args


class Driver:

    def __init__(self):
        self.args = parse_args()

    @staticmethod
    def main():
        driver = Driver()
        plot = Plot(driver.args.filename)

        # outputs a csv file with peaks (peaks_)
        print(f"trial filename \t {driver.args.filename}")
        raw_peaks_file = "peaks_" + driver.args.filename

        raw_peaks = fp.find_all_peaks_in_partition(driver.args.filename, raw_peaks_file)
        print(f"raw peaks file \t {raw_peaks_file}")

        # parses the time and vsum from peaks csv file
        # the returned values can be used as an input for plotting util

        raw_peaks_for_plotting = fp.get_peaks_for_plotting(raw_peaks)

        # returns time and acceleration, also creates two csv files
        # 1. resampled and interpolated data (si_)
        # 2. filtered columns (i.e.) just index and vsum (si_vsum_)
        # (siX, siY) = si.sampling_and_interpolation(driver.args.filename, '20ms')

        # outputs a csv file (peaks_)
        # si_peaks_file = "peaks_" + driver.args.filename
        # si_peaks = fp.find_all_peaks_in_partition('si_' + driver.args.filename, si_peaks_file)
        # print(f"si peaks file \t {si_peaks_file}")

        # si_peaks_for_plotting = fp.get_peaks_for_plotting(si_peaks)

        data_points = [[plot.time_list, plot.acc_list]]
        peak_points = [raw_peaks_for_plotting]

        plot.range_slider_plotly(data_points, peak_points, ["raw", "si"], ["raw", "si"])


if __name__ == "__main__":
    Driver.main()
