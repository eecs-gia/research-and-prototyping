import datetime as dt

import pandas as pd


def find_all_peaks_in_partition(file_name, write_file):
    """
        data format:
        2
        195, 2, 1574839747019, -1.087339, 6.710845, 7.041358, 9.787669
        ...
        userID
        index,userID,timeMs,accX,accY,accZ,vSum

        segmented gait data format:
        (78, 5, 1574587264032, -1.403481, 4.89542, 8.473579, 9.886174);
        (index,userID,timeMs,accX,accY,accZ,vSum)
    """

    Y_AXIS_THRESHOLD = 25.50  #
    SKIP_FRAMES = 35  # a good heuristics value for 50Hz can go up to 45

    peakFile = open('./' + write_file, 'w+')
    max_vsum = 0
    max_line_list = ''
    max_timestamp = 0
    last_timestamp = 0
    max_e2 = ''
    with open(file_name, 'r', encoding='utf-8-sig') as str_data:

        peaks = []
        try:
            peakFile.write(next(str_data))
            e1, e2 = next(str_data), next(str_data)
            is_peak = False
            for e3 in str_data:
                is_peak = False
                v1 = float((e1.split(',')[6]).strip())
                v2 = float((e2.split(',')[6]).strip())
                v3 = float((e3.split(',')[6]).strip())

                i2 = int((e2.split(',')[0]).strip())  # index of e2
                if (v1 < v2 and v3 < v2):
                    # it's some peak
                    peak_index = int((e2.split(',')[0]).strip())
                    line = e2.replace("\n", "").split(",")
                    index = line[0].strip()
                    userId = line[1].strip()
                    timestamp = line[2].strip()
                    accelerometer_x = line[3].strip()
                    accelerometer_y = line[4].strip()
                    accelerometer_z = line[5].strip()
                    vector_sum = line[6].strip()
                    line_list = [index, str(userId), timestamp, accelerometer_x, accelerometer_y, accelerometer_z,
                                    vector_sum]
                    if(int(last_timestamp) == 0):
                        last_timestamp = timestamp
                    if(float(vector_sum) > float(max_vsum)):
                        max_vsum = vector_sum
                        max_line_list = line_list
                        max_e2 = e2

                    if ((int(timestamp) - int(last_timestamp)) > 1000):
                        max_vsum = 0
                        last_timestamp = max_timestamp

                        peaks.append(max_line_list)
                        peakFile.write(max_e2)

                # move pointers
                e1 = e2
                e2 = e3
        except StopIteration:
            pass

        return peaks


# TODO save_to_disk feature
def get_peaks_for_plotting(peaks, save_to_disk=False):
    peaks_df = pd.DataFrame(data=peaks, columns=['index', 'userID', 'timeMs', 'accX', 'accY', 'accZ', 'vSum'])
    vsums = peaks_df['vSum'].tolist()
    timestamps = peaks_df['timeMs'].tolist()

    for index, timestamp in enumerate(timestamps):
        if '-' not in timestamp:
            time_ms = float(timestamp) / 1000.0
            time_ms_obj = dt.datetime.fromtimestamp(time_ms).strftime('%Y-%m-%d %H:%M:%S.%f')
            timestamps[index] = time_ms_obj
        else:
            timestamps[index] = timestamp

    return [timestamps, vsums]
