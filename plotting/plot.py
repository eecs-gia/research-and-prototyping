#!/usr/bin/env python

import csv
import datetime

import plotly.graph_objects as go


class Plot:
    filename = ""

    def __init__(self, filename):
        self.time_list = []
        self.acc_list = []
        self.x_list = None
        self.filename = filename
        self.parse_data()

    # index,userID,timeMs,accX,accY,accZ,vSum
    def parse_data(self):
        with open(self.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            for row in reader:
                v_sum = row['vSum']
                self.acc_list.append(v_sum)
                time_ms = row['timeMs'].strip()
                time_ms = float(time_ms) / 1000.0
                time_ms_obj = datetime.datetime.fromtimestamp(time_ms).strftime('%Y-%m-%d %H:%M:%S.%f')
                self.time_list.append(time_ms_obj)

    def range_slider_plotly(self, data_points, peak_points, labels_data_points, labels_peak_points):
        # Create figure
        fig = go.Figure()

        for index, data in enumerate(data_points):
            fig.add_trace(
                go.Scatter(x=data[0], y=data[1],
                           name='gait plot ' + labels_data_points[index]))

        for index, peaks in enumerate(peak_points):
            fig.add_trace(go.Scatter(x=peaks[0], y=peaks[1],
                                     mode='markers',
                                     name='peaks  ' + labels_peak_points[index]))

        # Set title
        fig.update_layout(
            title_text="Time series plot of vector sum of gait acceleration in x, y, z"
        )

        # Add range slider
        fig.update_layout(
            xaxis=go.layout.XAxis(
                rangeselector=dict(
                    buttons=list([
                        dict(count=1,
                             label="1m",
                             step="month",
                             stepmode="backward"),
                        dict(count=6,
                             label="6m",
                             step="month",
                             stepmode="backward"),
                        dict(count=1,
                             label="YTD",
                             step="year",
                             stepmode="todate"),
                        dict(count=1,
                             label="1y",
                             step="year",
                             stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                type="date"
            )
        )
        fig.show()
