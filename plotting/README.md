# Description

This folder, plotting, contains several scripts to generate csv outputs (i.e. data transformations) and for plotting the results of the transformations.

1. Find peaks for leg on which the smartphone is placed. Output file prefix: `peaks_`
2. Resample and interpolate raw data to `20ms`. Output file prefix: `si_`
where s stands for sampling and i stands for interpolation.
3. filter the sampled and interpolated file from step 2 to output only index, and vSum columns. Output prefix: `si_vsum`
4. Find peaks in resampled and interpolated data. Output prefix: `peaks_si_`.

Hence, `4` csv files are generated. For each execution. Previous files are replaced incase of same name.

# Run Instructions

    python3 driver.py [filename].csv

## Additional Notes
1. A plot will be generated upon execution. The plot contains raw signal as well as interpolated signal and the peaks for both. All the data is superimposed using different colours.
2. All output files and the raw data files follow the same format as below (except for `si_vsum` file):

        +-----------------------------------------------+
        | index, userID, timeMs, accX, accY, accZ, vSum |
        +-----------------------------------------------+

`si_vsum` file format:

        +-------------+
        | index, vSum |
        +-------------+
