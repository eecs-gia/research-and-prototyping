from pyspark import SparkConf, SparkContext, SparkFiles
from pyspark.streaming import StreamingContext
from pyspark.sql import Row,SQLContext
import re
import sys
import csv
from io import StringIO
import pandas as pd
import math

import dynamic_interpolation as di

# create spark configuration
conf = SparkConf()
conf.setAppName("GaitApp")
# create spark context with the above configuration
sc = SparkContext(conf=conf)
sc.setLogLevel("ERROR")

sys.path.insert(0,SparkFiles.getRootDirectory())
sc.addFile("./dynamic_interpolation.py")

samples = sc.textFile("/segmented_gait_data.csv")
rows = samples.map(di.sampling_and_interpolation)

for row in rows.take(rows.count()): 
    # print('length of row is ' + str(len(row)))
    # row = eval(row) # row is a list

    f1 = open('train_acc_x.txt', 'a+')
    f2 = open('train_acc_y.txt', 'a+')
    f3 = open('train_acc_z.txt', 'a+')
    f4 = open('train_id.txt', 'a+')

    f1.write(row[1] + '\n')
    f2.write(row[2] + '\n')
    f3.write(row[3] + '\n')
    f4.write(str(row[0]) + '\n')




# # start the streaming computation
# ssc.start()
# # wait for the streaming to finish
# ssc.awaitTermination()

f1.close()
f2.close()
f3.close()
f4.close()