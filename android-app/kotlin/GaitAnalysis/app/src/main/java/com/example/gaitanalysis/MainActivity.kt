package com.example.gaitanalysis

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import android.widget.ToggleButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException
import java.nio.charset.Charset
import java.util.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import java.io.FileWriter


class MainActivity : AppCompatActivity(), SensorEventListener {

    lateinit var sensorManager: SensorManager
    companion object {
        const val REQUEST_PERMISSION = 1
    }

    val samplingRate = 25000 // 25000 micro-seconds = 40 Hz
//    println(Environment.getExternalStorageDirectory())
//    var file: File = File(getExternalFilesDir(null) + "/gait.csv")


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        accelerometer_data.text = "x = ${event!!.values[0]}\n\n" +
                "y = ${event!!.values[1]}\n\n" +
                "z = ${event!!.values[2]}\n"

//        file.writeText("This will be written to the file!")

        try {
            when (event.sensor.getType()) {
                Sensor.TYPE_ACCELEROMETER -> accelerometer_data.text = (
                    String.format(
                        "%d; ACC; %f; %f; %f; %f; %f; %f\n",
                        event.timestamp,
                        event.values[0],
                        event.values[1],
                        event.values[2],
                        0f,
                        0f,
                        0f
                    )
                )
//                Sensor.TYPE_GYROSCOPE_UNCALIBRATED -> writer.write(
//                    String.format(
//                        "%d; GYRO_UN; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        event.values[3],
//                        event.values[4],
//                        event.values[5]
//                    )
//                )
//                Sensor.TYPE_GYROSCOPE -> writer.write(
//                    String.format(
//                        "%d; GYRO; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        0f,
//                        0f,
//                        0f
//                    )
//                )
//                Sensor.TYPE_MAGNETIC_FIELD -> writer.write(
//                    String.format(
//                        "%d; MAG; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        0f,
//                        0f,
//                        0f
//                    )
//                )
//                Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED -> writer.write(
//                    String.format(
//                        "%d; MAG_UN; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        0f,
//                        0f,
//                        0f
//                    )
//                )
//                Sensor.TYPE_ROTATION_VECTOR -> writer.write(
//                    String.format(
//                        "%d; ROT; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        event.values[3],
//                        0f,
//                        0f
//                    )
//                )
//                Sensor.TYPE_GAME_ROTATION_VECTOR -> writer.write(
//                    String.format(
//                        "%d; GAME_ROT; %f; %f; %f; %f; %f; %f\n",
//                        event.timestamp,
//                        event.values[0],
//                        event.values[1],
//                        event.values[2],
//                        event.values[3],
//                        0f,
//                        0f
//                    )
//                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val toggle: ToggleButton = findViewById(R.id.recordingButton)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // The toggle is enabled
                Toast.makeText(this, "button got checked", Toast.LENGTH_LONG).show()
                sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
                val registerListener = sensorManager.registerListener(
                    this,
                    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    samplingRate
                )
//                val contents = file.readText() // Read file
                val dir = "${Environment.getExternalStorageDirectory()}/gait_data"
                File(dir).mkdirs()
                var file = "gait.csv"
                var openFile = File("$dir/$file")
                openFile.writeText("hello\n", charset("utf-8"))
            } else {
                // The toggle is disabled
                Toast.makeText(this, "button got unchecked", Toast.LENGTH_LONG).show()
                sensorManager.unregisterListener(this);

            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION)
        } else {
            write()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                write()
            }
        }
    }

    private fun write() {
//        val dir = "${Environment.getExternalStorageDirectory()}/gait_data"
//        File(dir).mkdirs()
//        var file = "gait.csv"
//        var openFile = File("$dir/$file")
////        val file = "%1\$tY%1\$tm%1\$td%1\$tH%1\$tM%1\$tS.log".format(Date())
//        var file2 = File("$dir/$file")
////        file2.writeText("hello", Charset.defaultCharset())
////        File("$dir/$file").printWriter().use {
////            it.println("text")
////        }
//        try {
////            val writer = FileWriter("list.txt")
//            file2.writeText("hello", Charset.defaultCharset())
//        } catch (e: Exception) {
//            Log.e("WRITER","Exception: "+Log.getStackTraceString(e));
//        }

    }


}
