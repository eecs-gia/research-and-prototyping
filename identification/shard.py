import random

data = {'1': [], '2': [], '3': []}

with open('train_acc_x.txt') as acc_x, open('train_acc_y.txt') as acc_y, open('train_acc_z.txt') as acc_z, open(
        'train_id.txt') as ids:
    counter = 1
    for x, y, z, id in zip(acc_x, acc_y, acc_z, ids):
        id_strip = id.strip()
        x_strip = x.strip()
        y_strip = y.strip()
        z_strip = z.strip()
        counter += 1
        data[id_strip].append({'id': counter, 'x': x_strip, 'y': y_strip, 'z': z_strip})

for id in data.keys():
    xyz = data[id]
    test_records_length = int(len(xyz) / 10)
    test_records = random.sample(xyz, test_records_length)
    chosen_ids = [i['id'] for i in test_records]
    new_train_records = [i for i in xyz if i['id'] not in chosen_ids]
    new_train_records_length = len(new_train_records)
    try:
        assert ((new_train_records_length + test_records_length) == len(xyz))
    except AssertionError:
        print(
            'ID:{} ORIGINAL LENGTH:{} NEW_TRAIN LENGTH:{} TEST LENGTH:{}'.format(id, len(xyz), new_train_records_length,
                                                                                 test_records_length))
        exit(-1)
    print('ID:{} ORIGINAL LENGTH:{} NEW_TRAIN LENGTH:{} TEST LENGTH:{}'.format(id, len(xyz), new_train_records_length,
                                                                               test_records_length))
    with open('test_acc_x.txt', 'w') as acc_x, open('test_acc_y.txt', 'w') as acc_y, open('test_acc_z.txt',
                                                                                          'w') as acc_z, open(
            'test_id.txt', 'w') as ids:
        for each in test_records:
            acc_x.write(each['x'] + '\n')
            acc_y.write(each['y'] + '\n')
            acc_z.write(each['z'] + '\n')
            ids.write(str(id) + '\n')
    with open('train_acc_x_new.txt', 'w') as acc_x, open('test_acc_y_new.txt', 'w') as acc_y, open('test_acc_z_new.txt',
                                                                                                   'w') as acc_z, open(
            'train_id_new.txt', 'w') as ids:
        for each in new_train_records:
            acc_x.write(each['x'] + '\n')
            acc_y.write(each['y'] + '\n')
            acc_z.write(each['z'] + '\n')
            ids.write(str(id) + '\n')
