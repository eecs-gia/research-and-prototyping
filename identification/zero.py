import numpy as np
files = ['train_acc_x_new.txt', 'train_acc_y_new.txt', 'train_acc_z_new.txt', 'test_acc_x.txt', 'test_acc_y.txt', 'test_acc_z.txt']
for file in files:
    file_length = sum(1 for line in open(file))
    gyr_file_name = file.replace('acc', 'gyr')
    res = np.full((file_length, 128), 0.0)
    np.savetxt(gyr_file_name, res)
