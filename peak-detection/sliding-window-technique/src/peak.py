#!/usr/bin/env python
# usage : python3 dist-stats.py filename.csv city
import csv
import sys
from argparse import ArgumentParser
import statistics as stats
import matplotlib.pyplot as plt
import datetime

import plotly.graph_objects as go

# import pandas as pd

class FindPeak:
    # constants
    Y_AXIS_THRESHOLD = 1.55 # or 1.6
    SAMPLING_RATE = 40 # Hz
    STRIDE_INTERVAL_LOWER_LIM = 0.8
    STRIDE_INTERVAL_UPPER_LIM = 1.4
    FRAME_LOWER_LIM = 32 
    FRAME_UPPER_LIM = 56 

    # Constructor
    def __init__(self):
        self.args = self.parse_args()
        with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            self.acc_list = [row['app_sum'] for row in reader]
        self.x_list = None
        self.get_X_list()

    # create bar chart top-10 of restaurantCategoryDist 
    # x-axis: restaurant category, y-axis: represents frequency (#restaurants) in the category
    # def plot_bar_chart(self, x_list, y_list, xlabel, ylabel, title, output_path):
    #     # %matplotlib inline
    #     plt.style.use('ggplot')
    #     x_pos = [i for i, _ in enumerate(x_list)]

    #     # fig = plt.figure(figsize=(15, 8))

    #     # fig.suptitle('Stock price comparison 2007-2017', 
    #     #      fontsize=20)

    #     # ax1 = fig.add_subplot(231)
    #     # ax1.set_title('MSFT')

    #     # ax1.plot(x_list,
    #     #         y_list, 
    #     #         color='green')
                
    #     # ax1 = fig.add_axes()
    #     plt.plot(x_pos, y_list, color='mediumseagreen')
    #     plt.xlabel(xlabel)
    #     plt.ylabel(ylabel)
    #     plt.title(title)
    #     plt.subplots_adjust(bottom=0.3)
    #     plt.xticks(x_pos, x_list, rotation = 45, fontsize=8)
    #     plt.show()
    #     plt.savefig(output_path)
    #     print("\plot_bar_chart(): Bar Chart ready! See file at : {}\n".format(output_path))

# loop on read csv 
    def findAllPeaks(self):
        # print (len(self.x_list))
        SKIP_FRAMES = 20 # a good heuristics value
        first = ''
        third = ''
        index = 1 # skip first
        peak_list = []
        category_dict = {}
        with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            e1, e2, e3 = next(reader), next(reader), None
            count = 20
            for e3 in reader:

                if (float(e1['app_sum']) < float(e2['app_sum']) and float(e3['app_sum']) < float(e2['app_sum'])):
                    # it's some peak
                    if(float(e2['app_sum']) > FindPeak.Y_AXIS_THRESHOLD):
                        # it's a Real peak (since it's 'first' peak in the window)
                        # move the sliding window
                        peak_list.append( (index, self.x_list[index], e2['app_sum']) )
                        for i in range(SKIP_FRAMES):
                            next(reader, None)
                            index += 1
                index += 1
                e1 = e2
                e2 = e3
        # write peak list to a file
        with open('./peaks_found.csv', 'w+') as file:
            for (i, j, k) in peak_list:
                file.write(str(i) + " " + k + '\n')
        return peak_list

    def range_slider_ploty(self, x_list, y_list, peak_list=None):
        # Load data
        # df = pd.read_csv(self.args.filename)

        # Create figure
        fig = go.Figure()

        fig.add_trace(
            go.Scatter(x=x_list, y=y_list))

        if(peak_list):
            x_marker_list = [x[1] for x in peak_list]
            y_marker_list = [x[2] for x in peak_list]
            fig.add_trace(go.Scatter(x=x_marker_list, y=y_marker_list,
                        mode='markers',
                        name='markers'))

        # Set title
        fig.update_layout(
            title_text="Time series plot of gait acceleration"
        )

        # Add range slider
        fig.update_layout(
            xaxis=go.layout.XAxis(
                rangeselector=dict(
                    buttons=list([
                        dict(count=1,
                            label="1m",
                            step="month",
                            stepmode="backward"),
                        dict(count=6,
                            label="6m",
                            step="month",
                            stepmode="backward"),
                        dict(count=1,
                            label="YTD",
                            step="year",
                            stepmode="todate"),
                        dict(count=1,
                            label="1y",
                            step="year",
                            stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                type="date"
            )
        )

        fig.show()
    # argument parser
    def parse_args(self):
        parser = ArgumentParser()
        parser.add_argument("filename")
        args = parser.parse_args()
        return args

    def get_X_list(self):
        if(self.x_list):
            return self.x_list
        d = datetime.datetime.now()
        x_list = [d + datetime.timedelta(seconds=x*(1/FindPeak.SAMPLING_RATE)) for (x, _) in enumerate(self.acc_list, 1)]
        self.x_list = x_list
        return x_list

    def get_Y_list(self):
        return self.acc_list

    # Main Client
    @staticmethod
    def main():
        findPeak = FindPeak()

        xlabel = "Time 0.025s per data point"
        ylabel = "Vector sum acceleration (XYZ)"
        title = "Time series plot of gait acceleration"
        output_path = './output.png'
        x_list = findPeak.get_X_list()
        y_list = findPeak.get_Y_list()
        # findPeak.plot_bar_chart(x_list, y_list, xlabel, ylabel, title, output_path)
        peak_list = findPeak.findAllPeaks()
        findPeak.range_slider_ploty(x_list, y_list)
        findPeak.range_slider_ploty(x_list, y_list, peak_list)

if __name__ == "__main__":
    FindPeak.main()