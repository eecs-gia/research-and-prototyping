import tensorflow as tf
import argparse
import numpy as np
import os

# Loads the gait data from a given path.
def load_X(path):
    X_signals = []
    files = os.listdir(path)
    for my_file in files:
        fileName = os.path.join(path, my_file)
        file = open(fileName, 'r')
        X_signals.append(
            [np.array(cell, dtype=np.float32) for cell in [
                row.strip().split(' ') for row in file
            ]]
        )
        file.close()
        # X_signals = 6*totalStepNum*128
    return np.transpose(np.array(X_signals), (1, 2, 0))

# Loads the graph with the given file name.
def load_graph(frozen_graph_filename):
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def, name="prefix")
    return graph


# Prints the name of the operations and tuples in the graph.
def print_ops_tensors(graph):
    for op in graph.get_operations():
        print(op.name)

    for each in [op.values()[0] for op in graph.get_operations()]:
        print(each)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--frozen_graph", default="./frozenInferenceGraphIdentification.pb", type=str,
                        help="Frozen inference graph.")
    args = parser.parse_args()
    frozen_graph = args.frozen_graph

    graph = load_graph(frozen_graph)

    print_ops_tensors(graph)

    # Gets the input and output tensors by their names.
    x = graph.get_tensor_by_name('prefix/Placeholder:0')
    y = graph.get_tensor_by_name('prefix/Softmax:0')

    # Loads the input data.
    X_test = load_X('./dataident/test_short/record')

    # Runs inference with the frozen inference graph and passes our data as input.
    with tf.Session(graph=graph) as sess:
        y_out = sess.run(y, feed_dict={
            x: X_test
        })
        # Prints out the results. Each line is the result inferred for that corresponding line in the test file.
        for each in y_out:
            print(np.argmax(each) + 1)
