#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import datetime
import math
import numpy as np
import argparse
import os
from decimal import *
import ntpath
import sys

pd.options.mode.chained_assignment = None  # default='warn'

def main():
    parser = argparse.ArgumentParser(description='converts gait human database dataset to GIA compatible')
    parser.add_argument("-inp", help='path to input file', type=str)
    parser.add_argument("-out", help='path to output file', type=str)
    args = parser.parse_args()
    seen_set = set()
    for dirname, dirnames, filenames in os.walk(args.inp):
        # path to all filenames.
        for filename in filenames:
            if (all(x in filename.split('-') for x in ['rp'])) and filename[:-5] not in seen_set:
                seen_set.add(filename[:-5])
                file_in = os.path.join(dirname, filename)
                file_out = os.path.join(dirname + '/out', filename.split('-')[0] + '_raw.csv' )
                print(file_in)
                print(file_out)
                logic(file_in, file_out)


def logic(file_in, file_out):
    dataset = pd.read_csv(file_in, header=0)
    
    # choose columns
    d = dataset[['accelerometerTimestamp_sinceReboot.s.', 'motionUserAccelerationX.G.', 'motionUserAccelerationY.G.', 'motionUserAccelerationZ.G.']]
    # print(d.dtypes)
    # add userID

    userID = ntpath.basename(file_in).split('-')[0][4:]
    # print(userID)
    # sys.exit()
    userID = int(userID.strip()) + 118
    d['userID'] = userID

    # convert time to unix timestamp - not required currenlty
    # d['accelerometerTimestamp_sinceReboot.s.'] = d['accelerometerTimestamp_sinceReboot.s.'].apply(lambda t: int(datetime.datetime.strptime(t.strip(), '%Y-%m-%d %H:%M:%S.%f %z').timestamp()))

    # increment dataframe index
    d.index += 1

    # add dataframe index as column
    d.insert(0, 'index', d.index)

    # rename column's headers
    d = d.rename(columns={"accelerometerTimestamp_sinceReboot.s.": "timeMs", "deviceID.txt.": "userID", "motionUserAccelerationX.G.":"accX", "motionUserAccelerationY.G.":"accY", "motionUserAccelerationZ.G.":"accZ"})
 
    # convert ns time to ms
    # d['timeMs'] = d['timeMs'].apply(lambda x: int(x * (10 ** (Decimal(x).as_tuple().exponent*-1)) ))
    d['timeMs'] = d['timeMs'].apply(lambda x: int(x * (1000) ))

    # d.timeMs = d.timeMs/1000000

    # calculate vector sum
    d['vSum'] = np.sqrt( (d['accX'] * d['accX']) + (d['accY'] * d['accY']) + (d['accZ'] * d['accZ']) )

    # round all floats to 6 dp
    d.accX = d.accX.round(6)
    d.accY = d.accY.round(6)
    d.accZ = d.accZ.round(6)
    d.vSum = d.vSum.round(6)

    # define order
    d = d[['index','userID','timeMs','accX','accY','accZ','vSum']]

    # export to csv
    d.to_csv(file_out, index = False, header=True)
    
if __name__ == "__main__":
    main()