    # use python3 or above 

Step# 1 cycle segment gait data

    python segmentation-ol.py sensors_1574587262494.csv

This will output file : segmented_gait_data.csv

Step# 2 load segmented_gait_data.csv with overwrite command

    hdfs dfs -put -f segmented_gait_data.csv /

Step#3 run spark dynamic interpolation job

    spark-submit dynamic_interpolation_spark_job.py

This will append interpolated data to files:
train_acc_x.txt
train_acc_y.txt
train_acc_z.txt
train_id.txt

Data is now ready for ML !!