from io import StringIO
import pandas as pd
import math
import sys
import datetime

DATA_POINTS = 128  # per row for dataset#1
DECIMAL_PLACES = 8  # per row for dataset#1


def transform_tuple_list_to_table(tl):
    res = 'index,userID,timeMs,accX,accY,accZ,vSum\n'
    for t in tl:
        curr_row = ''
        for d in t:
            curr_row += str(d) + ','

        curr_row = curr_row[:-1]
        curr_row += '\n'
        res += curr_row
    return res


def sampling_and_interpolation(line):
    gait_data_row = line.split(";")
    curr_subject_data = []

    for data_point in gait_data_row:
        if (data_point.strip()):
            # data_point_tuple = (index,userID,timeMs,accX,accY,accZ,vSum)
            data_point_tuple = eval(data_point.strip())
            curr_subject_data.append(data_point_tuple)

    tabled_window = transform_tuple_list_to_table(curr_subject_data)

    data_io_str = StringIO(tabled_window)

    df = pd.read_csv(data_io_str, sep=",", squeeze=True)

    # print(df)
    userID = df['userID'].iloc[0]

    start_time = df['timeMs'].iloc[0]
    end_time = df['timeMs'].iloc[-1]

    # print(start_time)
    # print(end_time)

    time_diff = int(end_time) - int(start_time)

    fq = float(DATA_POINTS) / (time_diff / float(1000))

    ms = math.floor((float(1) / fq) * 1000)

    ms = str(ms) + 'ms'

    # print(ms)

    df['timeMs'] = pd.to_datetime(df['timeMs'], unit='ms')

    df_accX = df[['timeMs', 'accX']].copy()
    df_accY = df[['timeMs', 'accY']].copy()
    df_accZ = df[['timeMs', 'accZ']].copy()

    resampled_df_accX = df_accX.set_index('timeMs').resample(ms).mean().interpolate()
    resampled_df_accY = df_accY.set_index('timeMs').resample(ms).mean().interpolate()
    resampled_df_accZ = df_accZ.set_index('timeMs').resample(ms).mean().interpolate()

    # total discarded samples since ms has to be integer
    # print(resampled.size - DATA_POINTS) 

    resampled_df_accX = resampled_df_accX[:DATA_POINTS]
    resampled_df_accY = resampled_df_accY[:DATA_POINTS]
    resampled_df_accZ = resampled_df_accZ[:DATA_POINTS]

    dfList_accX = list(resampled_df_accX['accX'])
    dfList_accY = list(resampled_df_accY['accY'])
    dfList_accZ = list(resampled_df_accZ['accZ'])

    str_accX = ' '.join([str(round(s, DECIMAL_PLACES)) for s in dfList_accX])
    str_accY = ' '.join([str(round(s, DECIMAL_PLACES)) for s in dfList_accY])
    str_accZ = ' '.join([str(round(s, DECIMAL_PLACES)) for s in dfList_accZ])

    res = []
    res.append(userID)
    res.append(str_accX)
    res.append(str_accY)
    res.append(str_accZ)

    # returns [userID, str(acc_x values list), str(acc_y values list), str(acc_z values list)]
    return res


if __name__ == "__main__":
    f = open('segmented_gait_data.csv', 'r')
    gait_data = []
    c = 0
    for line in f:
        sampled_data = sampling_and_interpolation(line)
        # print(sampled_data)
        gait_data.append(sampled_data)
        if sys.argv[1] == "plot":
            filename = "user-" + str(gait_data[c][0]) + "-" + datetime.datetime.now().isoformat() + ".csv"
            out = open(filename, "w+")
            out.write("app_sum" + "\n")
            data = str(gait_data[c][1]).split(" ")
            for d in data:
                if d != "":
                    out.write(d + "\n")
            c = c + 1
