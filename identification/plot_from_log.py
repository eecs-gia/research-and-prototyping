import matplotlib.pyplot as plt

accuracys = []
indices = [i for i in range(1, 101)]

with open('log.txt', 'r') as log:
    for line in log:
        pos = line.find('accuracy:')
        accuracy_str = line[pos:]
        accuracy_str = accuracy_str.replace('accuracy: ', '')
        accuracys.append(float(accuracy_str) * 100)

plt.plot(indices, accuracys)
plt.ylabel('Accuracy')
plt.xlabel('Iteration')
plt.title('Learning Curve - Accuracy calculated at each iteration of Deep Learning')
plt.savefig('learning_curve.png')