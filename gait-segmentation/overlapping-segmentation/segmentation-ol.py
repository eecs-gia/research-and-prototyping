#!/usr/bin/env python
# usage : python3 dist-stats.py filename.csv city
import csv
import sys
from argparse import ArgumentParser
import statistics as stats
import matplotlib.pyplot as plt
import datetime
import subprocess
import os
import plotly.graph_objects as go

# import pandas as pd
peak_indexes_list = []

class FindPeak:
    # constants
    Y_AXIS_THRESHOLD = 1.55 # or 1.6
    SAMPLING_RATE = 50 # Hz
    # STRIDE_INTERVAL_LOWER_LIM = 0.8
    # STRIDE_INTERVAL_UPPER_LIM = 1.4
    # FRAME_LOWER_LIM = 32 
    # FRAME_UPPER_LIM = 56 

    # Constructor
    def __init__(self):
        self.args = self.parse_args()
        with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            self.acc_list = [row['vSum'] for row in reader]
        self.x_list = None
        self.get_X_list()

# loop on read csv 
    def findAllPeaks(self):
        # print (len(self.x_list))
        SKIP_FRAMES = 20 # a good heuristics value
        first = ''
        third = ''
        index = 1 # skip first
        peak_list = []
        category_dict = {}
        with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            e1, e2, e3 = next(reader), next(reader), None
            count = 20
            for e3 in reader:

                if (float(e1['vSum']) < float(e2['vSum']) and float(e3['vSum']) < float(e2['vSum'])):
                    # it's some peak
                    if(float(e2['vSum']) > FindPeak.Y_AXIS_THRESHOLD):
                        # it's a Real peak (since it's 'first' peak in the window)
                        # move the sliding window
                        peak_list.append( (index, self.x_list[index], e2['vSum'], e2['index']) )
                        peak_indexes_list.append(e2['index'])
                        for i in range(SKIP_FRAMES):
                            next(reader, None)
                            index += 1
                index += 1
                e1 = e2
                e2 = e3
        # write peak list to a file
        with open('./peaks_found.csv', 'w+') as file:
            for (i, j, k, l) in peak_list:
                file.write(k + " " + l + '\n')
        return peak_list

    # def gait_segmentation(self):
    #     # index,userID,timeMs,accX,accY,accZ,vSum

    #     # state 0 : write data until peak, no new line, go to state 1
    #     # state 1 : write data until peak, add new line, write same data again go to state 1

    #     # discard last line of file

    #     f1 = open("train_acc_x.txt", "a+")
    #     f2 = open("train_acc_y.txt", "a+")
    #     f3 = open("train_acc_z.txt", "a+")
    #     f4 = open("train_id.txt", "a+")
    #     with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
    #         reader = csv.DictReader(file)
    #         # discard data until first peak
    #         while(next(reader)["index"] != peak_indexes_list[0]):
    #                 pass
    #         ol1 = ''
    #         ol2 = ''
    #         ol3 = ''
    #         state = 0
    #         for i, peak in enumerate(peak_indexes_list[1:], 1):
    #             # print(i)
    #             # print(peak)
    #             data = next(reader)
    #             while(data["index"] != peak):
    #                 f1.write(data['accX'] + ' ')
    #                 f2.write(data['accY'] + ' ')
    #                 f3.write(data['accZ'] + ' ')
    #                 if(state == 1):
    #                     ol1 += data['accX'] + ' '
    #                     ol2 += data['accY'] + ' '
    #                     ol3 += data['accZ'] + ' '
    #                 data = next(reader)
    #             # if(data["index"] == peak):
    #             #     print("peak matched ln 92")
    #             if(state == 1):
    #                 f4.write(data['userID'] + '\n')
    #                 f1.write('\n')
    #                 f2.write('\n')
    #                 f3.write('\n')
    #                 f1.write(ol1)
    #                 f2.write(ol2)
    #                 f3.write(ol3)
    #                 ol1 = ''
    #                 ol2 = ''
    #                 ol3 = ''
    #             if(state == 0):
    #                 state = 1

    #     f1.close()
    #     f2.close()
    #     f3.close()
    #     f4.close()

    #     remove_last_line = "sed -i '' -e '$ d' {}"
    #     r1 = remove_last_line.format("train_acc_x.txt")
    #     res = subprocess.check_output([r1], shell=True)
    #     r2 = remove_last_line.format("train_acc_y.txt")
    #     res = subprocess.check_output([r2], shell=True)
    #     r3 = remove_last_line.format("train_acc_z.txt")
    #     res = subprocess.check_output([r3], shell=True)

    #     # truncate -s-1 file # to remove last char https://stackoverflow.com/questions/27305177/how-can-i-remove-the-last-character-of-a-file-in-unix
    

    def gait_segmentation(self):
        # index,userID,timeMs,accX,accY,accZ,vSum

        # state 0 : write data until peak, no new line, go to state 1
        # state 1 : write data until peak, add new line, write same data again go to state 1

        # discard last line of file

        f1 = open("segmented_gait_data.txt", "a+")
        with open(self.args.filename, 'r', encoding='utf-8-sig') as file:
            reader = csv.DictReader(file)
            # discard data until first peak
            while(next(reader)["index"] != peak_indexes_list[0]):
                    pass
            ol1 = ''
            state = 0
            for i, peak in enumerate(peak_indexes_list[1:], 1):
                # print(i)
                # print(peak)
                data = next(reader)
                # print(data)
                while(data["index"] != peak):
                    t_data = self.parse_dict_to_tuple(data)
                    t_data = str(t_data)
                    # tuple(data.split(','))

                    f1.write(t_data + '; ')
                    if(state == 1):
                        ol1 += t_data + '; '
                    data = next(reader)
                # if(data["index"] == peak):
                #     print("peak matched ln 92")
                if(state == 1):
                    # f1.seek(-1, os.SEEK_CUR)
                    # f1.seek(f1.tell() - 1, os.SEEK_SET)
                    f1.write('\n')
                    f1.write(ol1)
                    ol1 = '' # reset
                if(state == 0):
                    state = 1

        f1.close()

        remove_last_line = "sed -i '' -e '$ d' {}"
        r1 = remove_last_line.format("segmented_gait_data.txt")
        res = subprocess.check_output([r1], shell=True)

        # truncate -s-1 file # to remove last char https://stackoverflow.com/questions/27305177/how-can-i-remove-the-last-character-of-a-file-in-unix
    
    def parse_dict_to_tuple(self, data):
        t = tuple(data.values())
        res = []
        for val in t:
            val = eval(val.strip())
            res.append(val)
        # print(t)
        return tuple(res)

    def range_slider_ploty(self, x_list, y_list, peak_list=None):
        # Load data
        # Create figure
        fig = go.Figure()

        fig.add_trace(
            go.Scatter(x=x_list, y=y_list))

        if(peak_list):
            x_marker_list = [x[1] for x in peak_list]
            y_marker_list = [x[2] for x in peak_list]
            fig.add_trace(go.Scatter(x=x_marker_list, y=y_marker_list,
                        mode='markers',
                        name='markers'))

        # Set title
        fig.update_layout(
            title_text="Time series plot of gait acceleration"
        )

        # Add range slider
        fig.update_layout(
            xaxis=go.layout.XAxis(
                rangeselector=dict(
                    buttons=list([
                        dict(count=1,
                            label="1m",
                            step="month",
                            stepmode="backward"),
                        dict(count=6,
                            label="6m",
                            step="month",
                            stepmode="backward"),
                        dict(count=1,
                            label="YTD",
                            step="year",
                            stepmode="todate"),
                        dict(count=1,
                            label="1y",
                            step="year",
                            stepmode="backward"),
                        dict(step="all")
                    ])
                ),
                rangeslider=dict(
                    visible=True
                ),
                type="date"
            )
        )

        fig.show()
    # argument parser
    def parse_args(self):
        parser = ArgumentParser()
        parser.add_argument("filename")
        args = parser.parse_args()
        return args

    def get_X_list(self):
        if(self.x_list):
            return self.x_list
        d = datetime.datetime.now()
        x_list = [d + datetime.timedelta(seconds=x*(1/FindPeak.SAMPLING_RATE)) for (x, _) in enumerate(self.acc_list, 1)]
        self.x_list = x_list
        return x_list

    def get_Y_list(self):
        return self.acc_list

    # Main Client
    @staticmethod
    def main():
        findPeak = FindPeak()

        xlabel = "Time 0.025s per data point"
        ylabel = "Vector sum acceleration (XYZ)"
        title = "Time series plot of gait acceleration"
        output_path = './output.png'
        x_list = findPeak.get_X_list()
        y_list = findPeak.get_Y_list()
        # findPeak.plot_bar_chart(x_list, y_list, xlabel, ylabel, title, output_path)
        peak_list = findPeak.findAllPeaks()
        findPeak.gait_segmentation()
        # findPeak.range_slider_ploty(x_list, y_list)
        # findPeak.range_slider_ploty(x_list, y_list, peak_list)

if __name__ == "__main__":
    FindPeak.main()